class ArticlesController < ApplicationController

  #http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]

	def new
		print "ArticlesController.new.inicio \n"
		@article = Article.new
		print "ArticlesController.new.fin \n"
	end

	def create
  		
	print "ArticlesController.create.inicio \n"

	@article = Article.new(article_params)
	if @article.save
		print "ok save, redireccionando \n"
		redirect_to @article
	else
		print "Error Render to new \n"
		render 'new'
	end

	#render plain: params[:article].inspect

	print "ArticlesController.create.fin \n"
end

def show
	print "ArticlesController.show.inicio:", :id
	@article = Article.find(params[:id])
	print "ArticlesController.show.fin \n"
end

def index
	print "ArticlesController.index.inicio \n"
	@articles = Article.all
	print "ArticlesController.index.fin \n"
end

def edit
	@article = Article.find(params[:id])
end

def update 
	@article = Article.find(params[:id])
	print "ArticlesController.update.inicio @article  \n"

	if @article.update(article_params)
		redirect_to @article
	else
		render 'edit'
	end
	print "ArticlesController.update.fin \n"
end	

def destroy
  print "ArticlesController.destroy.inicio \n"
  @article = Article.find(params[:id])
  @article.destroy

  redirect_to articles_path
  print "ArticlesController.destroy.fin \n"
end

private def article_params
	params.require(:article).permit(:title, :text)
end

end
